<?php
/**
 * Created by PhpStorm.
 * User: Philipp Fedorov <svazist@gmail.com>
 * Date: 21.11.18
 * Time: 15:22
 */

class MyException extends \Exception {

}

try {

    throw new MyException("test");
} catch (Exception $ex) {
    print_r("Exceptin");
} catch (MyException $ex) {
    print_r("MyException");
}


$bu = TRUE;

echo ($bu++).$bu;

echo "\r\n";

class A {
    public $str = "str";
}
class B1 extends A {
    public $str = "string";
}

class B2 extends A {
    public $str = 0;
}

function m($c) {
    $c->str = "string";
}

$a1 = new A();
$a2 = new B2();

$b1 = new B1();
$b2 = new B2();

m($a1);
m($a2);
m($b1);
m($b2);

echo  $a1->str . PHP_EOL;
echo  $a2->str . PHP_EOL;
echo  $b1->str . PHP_EOL;
echo  $b2->str . PHP_EOL;